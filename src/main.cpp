#include <main.h>
#include <LedControl.h>

#define BUTTON_UP 2
#define BUTTON_DN 3
#define TIP120PIN 5

long debouncing_time = 15; //Debouncing Time in Milliseconds
volatile unsigned long last_micros;

uint8_t fanSpeed = 0;

//Parameters: SDA, SCL, CS, number of matrices
LedControl lc=LedControl(A4, A5, 4, 1);

bool checkBit(uint8_t value, uint8_t bit) {
	return ((value >> bit) & 1);
}

void singleLED(uint8_t x, uint8_t y, bool on) {
	lc.setLed(0, x, y, on);
} 

/**
 * @brief Displays a digit on the first matrix
 * 
 * @param digit the digit to be displayed (see `main.h`)
 * @param size the number of rows to display
 * @param rowOffset the number of rows to shift down
 * @param colOffset the number of columns to shift left
 */
void putDigit(byte *digit, size_t size, size_t rowOffset, size_t colOffset) {
	lc.clearDisplay(0);
	for (size_t row=0; row<size; row++) {
		if (row < 0) continue;
		for (size_t col=0; col<8; col++) {
			if (col < 0) continue;
			lc.setLed(0, col+colOffset, row+rowOffset, checkBit(digit[row], col));
		} 
	}
}

void debounceInterruptUp() {
	if((long)(micros() - last_micros) >= debouncing_time * 1000) {
		if (fanSpeed < 9) fanSpeed++;
		putDigit(digits[fanSpeed], 7, 1, 1);
		analogWrite(TIP120PIN, modes[fanSpeed].pwmValue);
		last_micros = micros();
	}
}

void debounceInterruptDn() {
	if((long)(micros() - last_micros) >= debouncing_time * 1000) {
		if (fanSpeed > 0) fanSpeed--;
		putDigit(digits[fanSpeed], 7, 1, 1);
		analogWrite(TIP120PIN, modes[fanSpeed].pwmValue);
		last_micros = micros();
	}
}

void setup() {
	Serial.begin(9600);
	//The MAX72XX is in power-saving mode on startup, we have to do a wakeup call
	lc.shutdown(0,false);
	//Set the brightness to a medium values
	lc.setIntensity(0,8);
	//and clear the display
	lc.clearDisplay(0);

	pinMode(TIP120PIN, OUTPUT);

	attachInterrupt(digitalPinToInterrupt(BUTTON_UP), debounceInterruptUp, LOW);
	attachInterrupt(digitalPinToInterrupt(BUTTON_DN), debounceInterruptDn, LOW);

	putDigit(digits[fanSpeed], 7, 1, 1);
}

void loop() {
}