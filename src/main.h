#pragma once

#include <Arduino.h>

struct FanSpeedMode {
	uint8_t displayValue;
	uint8_t pwmValue;
};

FanSpeedMode modes[] = {
	{0, 0},
	{1, 14},
	{2, 15},
	{3, 16},
	{4, 18},
	{5, 22},
	{6, 36},
	{7, 64},
	{8, 128},
	{9, 255}	
};

byte zero[] = {
	B001110,
	B010001,
	B010001,
	B010101,
	B010001,
	B010001,
	B001110
};

byte one[] = {
	B00100,
	B01100,
	B00100,
	B00100,
	B00100,
	B00100,
	B01110
};

byte two[] = {
	B01110,
	B10001,
	B00001,
	B01110,
	B10000,
	B10000,
	B11111
};

byte three[] = {
	B00001110,
	B00010001,
	B00000001,
	B00001110,
	B00000001,
	B00010001,
	B00001110
};

byte four[] = {
	B00001,
	B00011,
	B00101,
	B01001,
	B11111,
	B00001,
	B00001
};

byte five[] = {
	B11111,
	B10000,
	B10000,
	B01110,
	B00001,
	B10001,
	B01110
};

byte six[] = {
	B01111,
	B10000,
	B10000,
	B11110,
	B10001,
	B10001,
	B01110
};

byte seven[] = {
	B11111,
	B00001,
	B00010,
	B00100,
	B01000,
	B10000,
	B10000
};

byte eight[] = {
	B01110,
	B10001,
	B10001,
	B01110,
	B10001,
	B10001,
	B01110
};

byte nine[] = {
	B01110,
	B10001,
	B10001,
	B01111,
	B00001,
	B10001,
	B01110
};

byte *digits[] = {
	zero,
	one,
	two,
	three,
	four,
	five,
	six,
	seven,
	eight,
	nine
};
